const path = require('path');
const fs = require('fs');

var express = require('express');
var Server = require("http").Server;
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var morgan = require('morgan');
const {User, Room} = require('./models');
const roomPath = path.join(__dirname, './rooms');
const writeFile = require('write');
const mkdirp = require('mkdirp');
const lodash = require('lodash');
const removeGitIgnored = require('remove-git-ignored');
const archiver = require('archiver');

const readProject = require('./projectReader');

// redis
var redis = require("redis");
const redisClient = redis.createClient();

// invoke an instance of express application.
var app = express();

// set our application port
app.set('port', 3000);

// set morgan to log info about our requests for development use.
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
const sessisonMiddleware = session({
    store: new RedisStore({client: redisClient}),
    key: 'user_sid',
    secret: 'top_secret',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 1000 * 60 * 60 * 40,
        httpOnly: false
    }
});

app.use(sessisonMiddleware);

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');
    }
    next();
});

const permissionMiddleware = (req, res, next) => {
    if (!req.cookies.user_sid || !req.session.user) {
        res.status(403).json({message: 'У вас нет доступа'});
    }
    next();
};


// route for user signup
app.route('/api/signup')
    .post((req, res) => {
        console.log(req.body);
        User.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        })
            .then(user => {
                res.status(201).json();
            })
            .catch(error => {
                console.log(error.errors);
                res.status(400).json({message: ['Логин или email уже заняты']});
            });
    });


// route for user Login
app.route('/api/login')
    .post((req, res) => {
        const username = req.body.username,
            password = req.body.password;

        User.findOne({where: {username: username}}).then(function (user) {
            if (!(user && user.validPassword(password))) {
                res.status(400).json({message: ['Неверный логин или пароль']})
            } else {
                console.log(user);
                req.session.user = user;
                res.status(200).json(user)
            }
        });
    });

// route for user logout
app.post('/api/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.status(200).json({message: 'Вы успешно разлогинены'});
    } else {
        res.status(400).json({message: 'Вы не залогинены'});
    }
});


app.get('/api/init', permissionMiddleware, (req, res) => {
    res.json(req.session.user);
});


app.get('/api/users', permissionMiddleware, async (req, res) => {
    const users = await User.findAll();
    res.json({
        users: users
            .filter(model => model.dataValues.username !== req.session.user.username)
            .map(model => {
                return {id: model.dataValues.id, username: model.dataValues.username}
            })
    });
});


const iterOnFiles = async (filesPath, dict) => {
    if (dict === null) {
        // console.log(filesPath);
        mkdirp(filesPath, err => {
            if (err) {
                console.log(err);
            }
        });
        return;
    }
    Object.keys(dict).forEach(file => {
        if (typeof (dict[file]) === 'object') {
            iterOnFiles(path.join(filesPath, file), dict[file]);
        } else {
            if (/\ufffd/.test(dict[file]) === false) {
                // console.log(file);
                writeFile.promise(path.join(filesPath, file), dict[file]);
            }
        }
    });
    return Promise.resolve();
};


const simpleCreateProject = async (body, room_secret) => {
    const projectSize = lodash.size(body.project[lodash.keys(body.project)[0]]);
    if (projectSize > 0) {
        await iterOnFiles(path.join(roomPath, room_secret), body.project);
    } else {
        fs.mkdirSync(path.join(roomPath, room_secret, Object.keys(body.project)[0]))
    }
    removeGitIgnored({
        rootPath: path.join(roomPath, room_secret),
        isSilent: true
    });
};


app.route('/api/rooms')
    .get(permissionMiddleware, async (req, res) => {
        const user = await User.findOne({where: {username: req.session.user.username}});
        const rooms = await user.getRooms({order: [['id', 'ASC']], include: [{model: User, as: 'users'}]});
        res.json({
            rooms: rooms
        });
    })
    .post(permissionMiddleware, async (req, res) => {
        console.log(req.body);
        const room = await Room.create({
            name: req.body.name,
            description: req.body.description
        });

        const user = await User.findOne({where: {username: req.session.user.username}});
        const users = req.body.users;
        if (!users.map(user => user.id).includes(user.id)) {
            users.push(user);
        }

        try {
            await room.setUsers(users.map(user => user.id));
        } catch (err) {
            console.log(err);
            return res.status(400).json(err.errors);
        }

        console.log('room created');

        try {
            fs.mkdirSync(path.join(roomPath, room.secret));
        } catch (err) {
        }
        try {
            await simpleCreateProject(req.body, room.secret);
            const {id, name, description} = room;
            const payload = {id, name, description, users};
            res.status(201).json(payload);
        } catch (err) {
            res.status(400).json({message: 'create rooms error'});
        }
    });


app.route('/api/room/:roomId')
    .get(async (req, res) => {
        const roomId = req.params.roomId;
        const room = await Room.findOne({where: {id: roomId}, include: [{model: User, as: 'users'}]});

        if (!room.dataValues.users.includes(req.session.user.id)) {
            res.status(403).json({message: 'У вас нет доступа'});
        }

        const project = await new Promise(resolve => {
            readProject(path.join(roomPath, room.secret), 'utf-8', true, function (err, files) {
                if (err) throw err;
                resolve(files);
            });
        });
        res.status(200).json({...room, project})
    })
    .post(async (req, res) => {
        const room = await Room.findOne({where: {id: req.body.id}, include: [{model: User, as: 'users'}]});

        if (!room.dataValues.users.map(user => user.id).includes(req.session.user.id)) {
            return res.status(403).json({message: 'Форбиден'})
        }
        await room.update({
            name: req.body.name,
            description: req.body.description
        });

        const user = req.session.user;
        const users = req.body.users;
        if (!users.map(user => user.id).includes(user.id)) {
            users.push(user);
        }

        try {
            await room.setUsers(users.map(user => user.id));
        } catch (err) {
            console.log(err);
            return res.status(400).json(err.errors);
        }

        const {id, name, description} = room;
        const payload = {id, name, description, users};
        res.status(201).json(payload);
    })
    .delete(async (req, res) => {
        const roomId = req.params.roomId;
        const room = await Room.findOne({where: {id: roomId}, include: [{model: User, as: 'users'}]});

        if (!room.dataValues.users.map(user => user.id).includes(req.session.user.id)) {
            return res.status(403).json({message: 'Форбиден'})
        }

        if (room.dataValues.users.length === 1) {
            await room.destroy({force: true});
        } else {
            const users = room.dataValues.users.filter(user => user.id !== req.session.user.id);
            await room.setUsers(users.map(user => user.id))
        }
        return res.status(200).json({message: 'Успешно удалено'})
    });

app.get('/api/room/:roomId/zip', sessisonMiddleware, async (req, res) => {
    const roomId = req.params.roomId;
    const room = await Room.findOne({where: {id: roomId}, include: [{model: User, as: 'users'}]});

    const zipfile = archiver('zip');
    zipfile.pipe(res);

    zipfile.directory(path.join(roomPath, room.secret), '/').finalize();
});


// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
});


// start the express server
const server = app.listen(app.get('port'), () => console.log(`App started on port ${app.get('port')}`));

const io = require('socket.io')(server);

io.use((socket, next) => {
    sessisonMiddleware(socket.request, socket.request.res, next);
});


const EVENT_TYPES = {
    SUBSCRIBE_ROOM: 'subscribeRoom',
    CHANGE_CODE: 'changeCode',
};

io.sockets.on('connection', socket => {
    console.log('CONNECT');
    socket.on('message', async msg => {
        console.log(msg);
        let room;
        let roomId;
        switch (msg.type) {
            case EVENT_TYPES.SUBSCRIBE_ROOM:
                roomId = msg.payload.roomId;
                room = await Room.findOne({where: {id: roomId}, include: [{model: User, as: 'users'}]});
                if (!room.dataValues.users.map(user => user.id).includes(socket.request.session.user.id)) {
                    return;
                }

                socket.join(roomId);
                console.log('subscribe');

                const project = await new Promise(resolve => {
                    readProject(path.join(roomPath, room.secret), 'utf-8', true, function (err, files) {
                        if (err) throw err;
                        resolve(files);
                    });
                });

                const files = [];
                Object.keys(project).map(key => {
                    files.push({name: key, code: project[key]})
                });

                socket.emit('message', {
                    type: EVENT_TYPES.SUBSCRIBE_ROOM,
                    payload: {files}
                });
                break;

            case EVENT_TYPES.CHANGE_CODE:
                roomId = msg.payload.roomId;
                room = await Room.findByPk(roomId);
                const roomSecret = room.secret;
                const pathname = msg.payload.pathname;
                const filePath = path.join(roomPath, roomSecret, pathname);
                const code = msg.payload.code;
                // console.log(filePath,code);
                writeFile.sync(filePath, code);
                msg.payload.code = fs.readFileSync(filePath, {encoding: 'utf-8'});
                socket.to(roomId).emit('message', {
                    'type': EVENT_TYPES.CHANGE_CODE,
                    'payload': msg.payload
                });
                break;
        }
    })
});
