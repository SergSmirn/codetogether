var Sequelize = require('sequelize');
var bcrypt = require('bcrypt');

// create a sequelize instance with our local postgres database information.
var sequelize = new Sequelize('postgres://postgres@localhost:5432/fintech');

// setup Models model and its fields.
var User = sequelize.define('users', {
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    underscored: true,
    hooks: {
        beforeCreate: (user) => {
            const salt = bcrypt.genSaltSync();
            user.password = bcrypt.hashSync(user.password, salt);
        }
    }
});

User.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

var Room = sequelize.define('rooms', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: true
    },
    secret: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: true
    }
}, {
    underscored: true,
    hooks: {
        beforeCreate: (room) => {
            const salt = bcrypt.genSaltSync();
            room.secret = bcrypt.hashSync(room.name + Math.random(), salt).replace(/\//g, '-');
        }
    }
});

Room.belongsToMany(User, {through: 'user_room'});
User.belongsToMany(Room, {through: 'user_room'});

// create all the defined tables in the specified database.
sequelize.sync()
    .then(() => console.log('users table has been successfully created, if one doesn\'t exist'))
    .catch(error => console.log('This error occured', error));

// export Models model for use in other files.
module.exports = {User, Room};
