const path = require('path');
const fs = require('fs');

const async = require('async');
const errs = require('errs');

const readProject = function (dir, encoding, recursive, callback) {
    let result = {};

    callback = arguments[arguments.length - 1];
    typeof callback == "function" || (callback = null);

    if (typeof encoding == "boolean") {
        recursive = encoding;
        encoding = null;
    }
    typeof recursive == "undefined" && (recursive = true);
    typeof encoding == "string" || (encoding = null);

    fs.readdir(dir, function (err, entries) {
        if (err) {
            return errs.handle(err, callback);
        }

        async.forEach(entries, function (entry, next) {
            let entryPath = path.join(dir, entry);
            if (entry === '.git' || entry === '.idea') {
                return next();
            }
            fs.stat(entryPath, function (err, stat) {
                if (err) {
                    return next(err);
                }

                if (stat.isDirectory()) {
                    return (recursive || next()) && readProject(
                        entryPath,
                        encoding,
                        true,
                        function (err, entries) {
                            if (err) {
                                return next(err);
                            }

                            result[entry] = entries;
                            next();
                        }
                    );
                }
                fs.readFile(entryPath, encoding, function (err, content) {
                    if (err) {
                        return next(err);
                    }

                    result[entry] = content;
                    next();
                });
            });
        }, function (err) {
            callback && callback(err, result);
        });
    });
};

module.exports = readProject;
