import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'mobx-react';
import App from './App';
import 'normalize.css/normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import roomStore from './stores/roomStore';
import authStore from './stores/authStore';
import userStore from './stores/userStore';
import createRoomStore from "./stores/createRoomStore";
import editorStore from "./stores/editorStore"


const stores = {
    roomStore,
    authStore,
    userStore,
    createRoomStore,
    editorStore
};

ReactDOM.render((
    <Provider {...stores}>
        <Router>
            <App />
        </Router>
    </Provider>
), document.getElementById('root'));
