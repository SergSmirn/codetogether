import React, {Component} from 'react';
import './App.css';
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Editor from "./pages/Editor";
import Rooms from "./pages/Rooms";
import {withRouter, Route, Switch} from 'react-router-dom';
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import Header from "./components/Header/Header";
import {inject, observer} from 'mobx-react';
import cookie from "react-cookie";
import LoadingSpinner from "./components/Loader/Loader";


@inject('userStore')
@withRouter
@observer
class App extends Component {
    constructor(props) {
        super(props);
        if (!!cookie.load('user_sid')) {
            this.props.userStore.checkUser();
        }
    }

    render() {
        if (this.props.userStore.isLoading) {
            return <LoadingSpinner/>
        }
        return (<React.Fragment>
                <Header/>
                <div className="container app--content">
                    <div className="row justify-content-center app--content">
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/signup" component={Signup}/>
                            <Route exact path="/login" component={Login}/>
                            <PrivateRoute exact path="/room/:id" component={Editor}/>
                            <PrivateRoute exact path="/room" component={Rooms}/>
                        </Switch>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default App;
