import {Link} from 'react-router-dom';
import ErrorList from '../components/ErrorsList/ErrorsList';
import React from 'react';
import {inject, observer} from 'mobx-react';

@inject('authStore')
@observer
class Signup extends React.Component {
    componentWillUnmount() {
        this.props.authStore.reset();
    }

    handleUsernameChange = e => this.props.authStore.setUsername(e.target.value);
    handleEmailChange = e => this.props.authStore.setEmail(e.target.value);
    handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);
    handleSubmitForm = (e) => {
        e.preventDefault();
        this.props.authStore.signup()
            .then(() => this.props.history.replace('/login'));
    };

    render() {
        const {values, signupErrors, inProgress} = this.props.authStore;

        return (
            <div className="container signup--form">
                <div className="row justify-content-center">
                    <div className="col-md-6 card my-5">
                        <div className="card-body">
                            <h1 className="text-xs-center">Регистрация</h1>
                            <p className="text-xs-center">
                                <Link to="login">
                                    Уже есть аккаунт?
                                </Link>
                            </p>

                            <form onSubmit={this.handleSubmitForm}>
                                <fieldset>

                                    <fieldset className="form-group">
                                        <input
                                            className="form-control form-control-lg"
                                            type="text"
                                            placeholder="Логин"
                                            value={values.username}
                                            onChange={this.handleUsernameChange}
                                        />
                                    </fieldset>

                                    <fieldset className="form-group">
                                        <input
                                            className="form-control form-control-lg"
                                            type="email"
                                            placeholder="Email"
                                            value={values.email}
                                            onChange={this.handleEmailChange}
                                        />
                                    </fieldset>

                                    <fieldset className="form-group">
                                        <input
                                            className="form-control form-control-lg"
                                            type="password"
                                            placeholder="Пароль"
                                            value={values.password}
                                            onChange={this.handlePasswordChange}
                                        />
                                    </fieldset>

                                    <ErrorList errors={signupErrors}/>

                                    <button
                                        className="btn btn-lg btn-primary"
                                        type="submit"
                                        disabled={inProgress}>
                                        Зарегистрироваться
                                    </button>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Signup

