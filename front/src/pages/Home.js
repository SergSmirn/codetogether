import React, {Component} from 'react';

class Home extends Component {

    render() {
        return (
            <div>
                <hr/>
                <h1 className="text-center">Добро пожаловать</h1>
                <hr/>
                <div className="col-12">
                    <div className="row justify-content-around">
                        <div className="col-lg-4 col-sm-6 my-2">
                            <div className="bp3-card home--card">
                                <p className="home--icons"><i className="fas fa-sync-alt"></i></p>
                                <b>Синхронизация</b>
                                <br/>
                                Изменения вносимые в файлах отображаются у всех участников мгновенно. Интуитивно
                                понятный интерфейс редактора позволит увидеть изменения каждого члена команды.
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6 my-2">
                            <div className="bp3-card home--card">
                                <p className="home--icons"><i className="fas fa-hands-helping"></i></p>
                                <b>Взаимопомощь</b>
                                <br/>
                                Помогайте более юным разработчикам найти ошибку. Смотрите в режиме онлайн, как ваши
                                ученики пишут код. Обменивайтесь опытом со своими коллегами.
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6 my-2">
                            <div className="bp3-card home--card">
                                <p className="home--icons"><i className="fas fa-users"></i></p>
                                <b>Совместное программирование</b>
                                <br/>
                                Загружайте свой проект и дописывайте код все вместе и одновременно. Ваш проект надежно
                                хранится на наших серверах. В любой момент его можно сохранить на свой компьютер.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
