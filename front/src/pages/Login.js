import {withRouter, Link} from 'react-router-dom';
import ErrorsList from '../components/ErrorsList/ErrorsList';
import React from 'react';
import {inject, observer} from 'mobx-react';

@inject('authStore')
@withRouter
@observer
class Login extends React.Component {

    componentWillUnmount() {
        this.props.authStore.reset();
    }

    handleUsernameChange = e => this.props.authStore.setUsername(e.target.value);
    handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);

    handleSubmitForm = (e) => {
        e.preventDefault();
        this.props.authStore.login()
            .then(() => this.props.history.replace('/'));
    };

    render() {
        const {values, loginErrors, inProgress} = this.props.authStore;

        return (<div className="container login--form">
                <div className="row justify-content-center">
                    <div className="col-md-6 my-5 card">
                        <div className="card-body">
                            <h1 className="text-xs-center">Войти</h1>
                            <p className="text-xs-center">
                                <Link to="signup">
                                    Еще нет аккаунта?
                                </Link>
                            </p>
                            <form onSubmit={this.handleSubmitForm}>
                                <fieldset>
                                    <fieldset className="form-group">
                                        <input
                                            className="form-control form-control-lg"
                                            type="text"
                                            placeholder="Введите логин"
                                            value={values.username}
                                            onChange={this.handleUsernameChange}/>
                                    </fieldset>

                                    <fieldset className="form-group">
                                        <input
                                            className="form-control form-control-lg"
                                            type="password"
                                            placeholder="Введите пароль"
                                            value={values.password}
                                            onChange={this.handlePasswordChange}/>
                                    </fieldset>

                                    <ErrorsList errors={loginErrors}/>

                                    <button
                                        className="btn btn-lg btn-primary"
                                        type="submit"
                                        disabled={inProgress}>
                                        Войти
                                    </button>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login
