import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {Dialog} from '@blueprintjs/core'
import RoomList from '../components/RoomList'
import CreateRoom from "../components/CreateRoom/CreateRoom";
import LoadingSpinner from "../components/Loader/Loader";


@inject('roomStore', 'createRoomStore')
@observer
class Rooms extends Component {
    constructor() {
        super();
        this.state = {isOpen: false};
    }
    componentDidMount() {
        this.props.roomStore.loadRooms();
    }

    componentWillUnmount() {
        this.props.roomStore.reset();
    }

    handleOnAdd = () => {
        this.setState({isOpen: true});
    };

    handleClose = () => {
        this.setState({isOpen: false});
        this.props.createRoomStore.clearForm();
    };

    render() {
        const {rooms, isLoading} = this.props.roomStore;
        if (isLoading)
            return <LoadingSpinner/>;

        return (
            <div className="col-md-12 mb-5 mt-4">
                <button onClick={this.handleOnAdd} className="btn btn-block btn-outline-success">Добавить</button>
                <RoomList
                    rooms={rooms}
                    loading={isLoading}
                />
                <Dialog autoFocus={true} canEscapeKeyClose={true} canOutsideClickClose={true} enforceFocus={true}
                isOpen={this.state.isOpen} onClose={this.handleClose}>
                    <CreateRoom onClose={this.handleClose}/>
                </Dialog>
            </div>
        );
    }
}

export default Rooms;
