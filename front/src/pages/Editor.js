import React, {Component} from 'react';
import AceEditor from 'react-ace';
import {withRouter} from 'react-router-dom';
import openSocket from "socket.io-client";
import Sidebar from "../components/Sidebar/Sidebar";
import {inject, observer} from "mobx-react";

import 'brace/mode/python';
import 'brace/mode/java';
import 'brace/mode/jsx';
import 'brace/mode/c_cpp';
import 'brace/mode/csharp';
import 'brace/mode/css';
import 'brace/mode/tsx';
import 'brace/mode/html';
import 'brace/theme/github';


@inject('editorStore')
@withRouter
@observer
class Editor extends Component {
    constructor() {
        super();
        this.socket = openSocket('/');
    }

    componentDidMount() {
        this.socket.on('message', ({payload, type}) => {
            if (type === 'subscribeRoom') {
                this.props.editorStore.addFiles(payload.files);
            }
        });
        this.socket.on('message', ({type, payload}) => {
            if (type === 'changeCode') {
                this.props.editorStore.changeCode(payload.pathname, payload.code);
            }
        });
        this.socket.emit('message', {type: 'subscribeRoom', payload: {roomId: this.props.match.params.id}})
    }

    componentWillUnmount() {
        this.props.editorStore.reset();
        this.socket.destroy();
    }

    onChange = (value) => {
        const selectedFile = this.props.editorStore.selectedFile;
        this.socket.emit('message', {
            type: 'changeCode',
            payload: {code: value, roomId: this.props.match.params.id, pathname: selectedFile.name}
        });
    };

    handleFileClick = (e) => {
        this.props.editorStore.selectFile(e.target.textContent);
    };

    render() {
        const files = this.props.editorStore.allFiles;
        const selectedFile = this.props.editorStore.selectedFile;
        return (<React.Fragment>
                <div className="col-lg-2 col-md-12 my-4 sidebar--block">
                    <button className="btn btn-block mb-1">
                        <a href={`/api/room/${this.props.match.params.id}/zip`}>
                            Скачать
                        </a>
                    </button>
                    <Sidebar onClick={this.handleFileClick} files={files}/>
                </div>
                <div className="col-lg-10 col-md-12 my-4 app--content">
                    <AceEditor enableBasicAutocompletion={true}
                               style={{width: '100%', height: '100%'}} mode={selectedFile ? getLanguage(selectedFile.name) : ''}
                               theme="github"
                               value={selectedFile ? selectedFile.code : 'Выберите файл...'}
                               onChange={this.onChange}/>
                </div>
            </React.Fragment>
        );
    }
}

export default Editor;

const langByType = {
    py: 'python',
    js: 'jsx',
    css: 'css',
    c: 'c_cpp',
    cpp: 'c_cpp',
    ts: 'tsx',
    java: 'java',
    html: 'html'
};

function getLanguage(filename) {
    const type = filename.split('.')[1];
    const lang = langByType[type];
    if (lang) {
        return lang;
    } else {
        return ''
    }
}
