import axios from 'axios';


const Auth = {

    signup: (data) => {
        return axios.post(`/api/signup`, data)
            .then(response => {
                return response.data
            })
            .catch(error => Promise.reject(error.response.data.message));
    },

    login: (data) => {
        return axios.post(`/api/login`, data)
            .then(response => response.data)
            .catch(error => Promise.reject(error.response.data.message))
    },

    logout: () => {
        return axios.post(`/api/logout`, {})
            .then(response => {
                return response.data
            })
            .catch(error => Promise.reject(error.response.data.message));
    },

    init: () => {
        return axios.get(`/api/init`)
            .then(response => response.data)
            .catch(error => Promise.reject(error.response.data.message))
    }

};

const Rooms = {

    create: (data) => {
        return axios.post(`/api/rooms`, data)
            .then(response => {
                return response.data
            })
            .catch(error => Promise.reject(error.response.data.message));
    },

    update: (data) => {
      return axios.post('/api/room/' + data.id, data)
          .then(response => response.data)
          .catch(error => Promise.reject(error.response.data.message))
    },

    delete: (id) => {
        return axios.delete('/api/room/' + id)
            .then(response => response.data)
            .catch(error => Promise.reject(error.response.data.message))
    },

    list: () => {
        return axios.get(`/api/rooms`)
            .then(response => response.data)
            .catch(error => Promise.reject(error.response.data.message))
    },

    users: () => {
        return axios.get(`/api/users`)
            .then(response => response.data)
            .catch(error => Promise.reject(error.response.data.message))
    }

};

export default {
    Auth,
    Rooms
}

