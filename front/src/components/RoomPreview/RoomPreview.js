import React from 'react';
import {Link} from 'react-router-dom';
import './RoomPreview.css'
import {inject, observer} from "mobx-react";


@inject('roomStore')
@observer
class RoomPreview extends React.Component {
    onEditHandler = () => {
        this.props.roomStore.setEditing(this.props.room.id);
    };

    onDeleteHandler = () => {
        this.props.roomStore.deleteRoom(this.props.room.id);
    };

    render() {
        const {room} = this.props;

        return (<div className="room--preview col-lg-4 col-sm-6">
                <div className="bp3-card bp3-elevation-3 bp3-interactive">
                    <div className="row">
                        <div className="col-9">
                            <Link className="card-link" to={`/room/${room.id}`}>
                                <h5>{room.name}</h5>
                            </Link>
                        </div>
                        <div className="col-3">
                            <div className="row justify-content-end">
                                <div onClick={this.onEditHandler} className="room--preview__icon col-auto px-1">
                                    <i className="fas fa-pencil-alt"/>
                                </div>
                                <div onClick={this.onDeleteHandler} className="room--preview__icon col-auto pl-1">
                                    <i className="fas fa-trash"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-body room--preview__description">
                        {room.description || 'Здесь может быть Ваша реклама'}
                    </div>
                    <hr/>
                    <div className="">
                        <div className="row justify-content-around">
                            {room.users.map((user, index) => <div key={index}
                                                                  className="col-auto">{user.username}</div>)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default RoomPreview;