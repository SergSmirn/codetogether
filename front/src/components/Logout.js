import React, {Component} from 'react';
import {inject, observer} from "mobx-react";


@inject('authStore')
@observer
class Logout extends Component {
    onClick = e => {
        e.preventDefault();
        this.props.authStore.logout()
    };

    render() {
        return (
            <button className="btn btn-secondary logout-btn" onClick={this.onClick}>Выйти</button>
        );
    }
}

export default Logout;
