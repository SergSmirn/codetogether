import React from 'react';
import {inject, observer} from 'mobx-react';
import {MultiSelect} from "@blueprintjs/select";
import {MenuItem} from "@blueprintjs/core";
import './RoomEdit.css'


@inject('createRoomStore', 'roomStore')
@observer
class RoomEdit extends React.Component {
    constructor(props) {
        super(props);

        const selectedUsers = this.props.room.users.map(user => {
            return {username: user.username, id: user.id}
        });

        this.state = {
            room: {...this.props.room},
            selectedUsers: selectedUsers,
            error: false
        }
    }

    componentDidMount() {
        if (!this.props.createRoomStore.allUsers.length) {
            this.props.createRoomStore.loadUsers();
        }
    }

    validateName = (name) => {
        return !!name
    };

    handleNameChange = event => {
        if (!event.target.value) {
            this.setState({
                error: true,
                room: {...this.state.room, [event.target.name]: event.target.value}
            });
            return
        }

        this.setState({
            error: false,
            room: {...this.state.room, [event.target.name]: event.target.value}
        });
    };

    handleDescriptionChange = event => {
        this.setState({
            room: {...this.state.room, [event.target.name]: event.target.value}
        });
    };

    onSubmit = (event) => {
        event.preventDefault();

        if (!this.validateName(this.state.room.name)) {
            this.setState({error: true});
            return;
        }

        const newRoom = {...this.state.room, users: this.state.selectedUsers};
        this.props.roomStore.updateRoom(newRoom);
    };

    onClose = () => {
        this.props.roomStore.hideEditing(this.state.room.id)
    };

    isSelected = item => this.state.selectedUsers.map(user => user.id).includes(item.id);

    renderItem = (item, {handleClick, modifiers, query}) => {
        if (!modifiers.matchesPredicate) {
            return null;
        }

        return (
            <MenuItem
                label={this.highlightText(`id: ${item.id}`, query)}
                icon={this.isSelected(item) ? "tick" : "blank"}
                key={item.id}
                onClick={handleClick}
                text={this.highlightText(item.username, query)}
            />
        );
    };

    highlightText = (text, query) => {
        let lastIndex = 0;
        const words = query
            .split(/\s+/)
            .filter(word => word.length > 0);

        if (words.length === 0) {
            return [text];
        }
        const regexp = new RegExp(words.join("|"), "gi");
        const tokens = [];
        while (true) {
            const match = regexp.exec(text);
            if (!match) {
                break;
            }
            const length = match[0].length;
            const before = text.slice(lastIndex, regexp.lastIndex - length);
            if (before.length > 0) {
                tokens.push(before);
            }
            lastIndex = regexp.lastIndex;
            tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
        }
        const rest = text.slice(lastIndex);
        if (rest.length > 0) {
            tokens.push(rest);
        }
        return tokens;
    };

    filterItem = (query, item) => {
        return `${item.username} ${item.id}`.indexOf(query.toLowerCase()) >= 0;
    };

    renderTag = (item) => {
        return (
            <div key={item.id}>{item.username}</div>
        );
    };

    onItemSelect = (item) => {
        if (this.isSelected(item)) {
            const newSelectedUsers = this.state.selectedUsers.filter(user => user.id !== item.id);
            this.setState({selectedUsers: newSelectedUsers})
        } else {
            this.setState({selectedUsers: [...this.state.selectedUsers, item]});
        }
    };

    handleTagRemove = (tag) => {
        const newSelectedUsers = this.state.selectedUsers.filter(user => user.id !== parseInt(tag.key));
        this.setState({selectedUsers: newSelectedUsers})
    };

    render() {
        return (
            <div className="col-lg-4 col-sm-6">
                <div className="room--preview bp3-card bp3-elevation-3 bp3-interactive">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Название:</label>
                            <input type="text" className={this.state.error ? 'form-control is-invalid' : 'form-control'}
                                   name="name" value={this.state.room.name}
                                   onChange={this.handleNameChange}/>
                            {this.state.error ?
                                <div className="invalid-feedback">
                                    Это поле обязательно
                                </div> : ''}
                        </div>
                        <div className="form-group">
                            <label>Описание:</label>
                            <textarea className="form-control" name="description"
                                      value={this.state.room.description}
                                      onChange={this.handleDescriptionChange}/>
                        </div>
                        <div className="form-group">
                            <label>Участники:</label>
                            <MultiSelect className="room--form__user-select" itemPredicate={this.filterItem}
                                         selectedItems={this.state.selectedUsers} resetOnSelect={true}
                                         items={this.props.createRoomStore.allUsers}
                                         noResults={<MenuItem disabled={true} text="Ничего не найдено"/>}
                                         itemRenderer={this.renderItem} onItemSelect={this.onItemSelect}
                                         tagRenderer={this.renderTag} placeholder={'Логин или ID'}
                                         tagInputProps={{onRemove: this.handleTagRemove}}/>
                        </div>
                        <div className="container">
                            <div className="row justify-content-between">
                                <button type="submit"
                                        className="btn btn-primary btn-outline-primary">Сохранить
                                </button>
                                <button onClick={this.onClose} type="button"
                                        className="btn btn-secondary btn-outline-secondary">Закрыть
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default RoomEdit;
