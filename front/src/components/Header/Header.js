import React from 'react';
import {Link} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import Logout from "../Logout";

const LoggedOutView = props => {
    if (!props.currentUser) {
        return (
            <React.Fragment>
                <ul className="nav navbar-nav mr-auto flex-row">
                    <li className="nav-item mx-1">
                        <Link to="/" className="nav-link">
                            Главная
                        </Link>
                    </li>
                </ul>
                <ul className="nav navbar-nav  flex-row">
                    <li className="nav-item mx-1">
                        <Link to="/login" className="nav-link">
                            <button className="btn btn-primary">Войти</button>
                        </Link>
                    </li>

                    <li className="nav-item mx-1">
                        <Link to="/signup" className="nav-link">
                            <button className="btn btn-success">Присоединиться</button>
                        </Link>
                    </li>
                </ul>
            </React.Fragment>
        );
    }
    return null;
};

const LoggedInView = props => {
    if (props.currentUser) {
        return (
            <React.Fragment>
                <ul className="nav navbar-nav mr-auto  flex-row">
                    <li className="nav-item mx-1">
                        <Link to="/" className="nav-link">
                            Главная
                        </Link>
                    </li>
                    <li className="nav-item mx-1">
                        <Link to="/room" className="nav-link">
                            <i className="ion-compose"/>&nbsp;Комнаты
                        </Link>
                    </li>
                </ul>
                <ul className="nav navbar-nav  flex-row">
                    <div className="nav-item nav-link my-auto p-2">
                        {props.currentUser.username}
                    </div>

                    <li className="nav-item nav-link mx-1">
                        <Logout/>
                    </li>
                </ul>
            </React.Fragment>
        );
    }

    return null;
};

@inject('userStore')
@observer
class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                <Link to="/" className="navbar-brand">
                    CodeTogether
                </Link>

                <LoggedOutView currentUser={this.props.userStore.currentUser}/>

                <LoggedInView currentUser={this.props.userStore.currentUser}/>
            </nav>
        );
    }
}

export default Header;
