import React from 'react';
import {MenuItem, Menu} from "@blueprintjs/core";


class Sidebar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Menu>
                {this.props.files.map(file => {
                    return <MenuItem active={file.isSelected} onClick={this.props.onClick} key={file.name} text={file.name}/>
                })}
            </Menu>
        );
    }
}

export default Sidebar
