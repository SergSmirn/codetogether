import React from 'react';
import {inject, observer} from 'mobx-react';
import {MultiSelect} from "@blueprintjs/select";
import {MenuItem} from "@blueprintjs/core";
import Dropzone from 'react-dropzone'
import './CreateRoom.css'


@inject('createRoomStore')
@observer
class CreateRoom extends React.Component {
    componentDidMount() {
        this.props.createRoomStore.loadUsers();
    }

    handleNameChange = (e) => {
        this.props.createRoomStore.setName(e.target.value);
    };

    handleDescriptionChange = (e) => {
        this.props.createRoomStore.setDescription(e.target.value);
    };

    onSubmit = (event) => {
        event.preventDefault();
        if (!this.props.createRoomStore.values.name) {
            this.props.createRoomStore.errors = true;
            return;
        }

        this.props.createRoomStore.postForm()
            .then(() => {
                this.props.onClose();
            });
    };

    onDrop = async (accFiles) => {
        const project = {};
        await new Promise(resolve => {
            accFiles.forEach(async (file, index) => {
                const fileReader = new FileReader();
                fileReader.readAsText(file);
                while (fileReader.readyState !== 2) {
                    await __delay__(100);
                }
                project[file.name] = fileReader.result;
                if (index === accFiles.length -1) {
                    resolve();
                }
            });
        });

        this.props.createRoomStore.addFiles(accFiles);
        this.props.createRoomStore.addProject(project);
    };

    renderItem = (item, {handleClick, modifiers, query}) => {
        if (!modifiers.matchesPredicate) {
            return null;
        }

        return (
            <MenuItem
                label={this.highlightText(`id: ${item.id}`, query)}
                icon={this.props.createRoomStore.isUserSelected(item) ? "tick" : "blank"}
                key={item.id}
                onClick={handleClick}
                text={this.highlightText(item.username, query)}
            />
        );
    };

    highlightText = (text, query) => {
        let lastIndex = 0;
        const words = query
            .split(/\s+/)
            .filter(word => word.length > 0);

        if (words.length === 0) {
            return [text];
        }
        const regexp = new RegExp(words.join("|"), "gi");
        const tokens = [];
        while (true) {
            const match = regexp.exec(text);
            if (!match) {
                break;
            }
            const length = match[0].length;
            const before = text.slice(lastIndex, regexp.lastIndex - length);
            if (before.length > 0) {
                tokens.push(before);
            }
            lastIndex = regexp.lastIndex;
            tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
        }
        const rest = text.slice(lastIndex);
        if (rest.length > 0) {
            tokens.push(rest);
        }
        return tokens;
    };

    filterItem = (query, item) => {
        return `${item.username} ${item.id}`.indexOf(query.toLowerCase()) >= 0;
    };

    renderTag = (item) => {
        return (
            <div key={item.id}>{item.username}</div>
        );
    };

    onItemSelect = (item) => {
        if (this.props.createRoomStore.isUserSelected(item)) {
            this.props.createRoomStore.removeUser(item.id)
        } else {
            this.props.createRoomStore.addUser(item);
        }
    };

    handleTagRemove = (tag) => {
        this.props.createRoomStore.removeUser(parseInt(tag.key))
    };

    render() {
        const renderFiles = (files) => {
            return files.map((file, index) =>
                <div key={index}>{file.name} - {file.size / 1000} Kb</div>
            )
        };
        const {values, inProgress, errors, files} = this.props.createRoomStore;
        const selectedUsers = this.props.createRoomStore.selectedUsers;
        const users = this.props.createRoomStore.allUsers;
        return (
            <div className="container">
                <div className="col-12 my-4">

                    <h3>Добавить комнату</h3>

                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Название:</label>
                            <input type="text" className={errors ? "form-control is-invalid" : "form-control"}
                                   name="name" value={values.name}
                                   onChange={this.handleNameChange}/>
                            {errors ?
                                <div className="invalid-feedback">
                                    Это поле обязательно
                                </div> : ''}
                        </div>
                        <div className="form-group">
                            <label>Описание:</label>
                            <textarea className="form-control" name="description"
                                      value={values.description}
                                      onChange={this.handleDescriptionChange}/>
                        </div>
                        <div className="form-group">
                            <label>Участники:</label>
                            <MultiSelect className="room--form__user-select" itemPredicate={this.filterItem}
                                         selectedItems={selectedUsers} resetOnSelect={true}
                                         items={users} noResults={<MenuItem disabled={true} text="Ничего не найдено"/>}
                                         itemRenderer={this.renderItem} onItemSelect={this.onItemSelect}
                                         tagRenderer={this.renderTag} placeholder={'Логин или ID'}
                                         tagInputProps={{onRemove: this.handleTagRemove}}/>
                        </div>
                        <div className="form-group">
                            <Dropzone className="room--form__drop-file" activeClassName="room--form__drop-file__accept" maxSize={2000000} onDrop={this.onDrop}>
                                {files.length ? renderFiles(files) : <div>Кликните или перетащите сюда файлы</div>}
                            </Dropzone>
                        </div>
                        <div className="container">
                            <div className="row justify-content-between">
                                <button disabled={inProgress} type="submit"
                                        className="btn btn-primary btn-outline-primary">Добавить
                                </button>
                                <button onClick={this.props.onClose} type="button"
                                        className="btn btn-secondary btn-outline-secondary">Закрыть
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}


function __delay__(time) {
    return new Promise(resolve => {
        setTimeout(function () {
            resolve();
        }, time);
    });
}


export default CreateRoom;
