import React from 'react';
import './ErrorList.css'

const ErrorsList = ({errors}) => {
    if (errors) {
        return (
            <div className="error-list">
                {
                    Object.keys(errors).map(key => {
                        return (
                            <div className="error-message" key={key}>
                                {errors[key]}
                            </div>
                        );
                    })
                }
            </div>
        );
    } else {
        return null;
    }
};

export default ErrorsList

