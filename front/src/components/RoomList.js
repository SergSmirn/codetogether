import React from 'react';
import RoomPreview from "./RoomPreview/RoomPreview";
import RoomEdit from "./RoomEdit/RoomEdit";

const RoomList = ({rooms}) => {

    if (rooms.length === 0) {
        return (
            <div className="row justify-content-center article-preview my-5">
                Здесь ещё ничего нет. Вы можете добавить новую комнату.
            </div>
        );
    }

    return (
        <div className="row justify-content-start">
            {rooms.map((room, index) => {
                if (room.isEditing) {
                    return (<RoomEdit room={room} key={index}/>)
                }
                return (<RoomPreview room={room} key={index}/>
                );
            })
            }
        </div>
    );
};

export default RoomList;
