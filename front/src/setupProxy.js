const setupProxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(setupProxy('/api', { target: 'http://localhost:3000/' }));
    app.use(setupProxy('/socket.io', { target: 'http://localhost:3000/' }));
};
