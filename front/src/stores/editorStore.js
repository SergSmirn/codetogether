import {observable, action, computed} from 'mobx';


class editorStore {
    @observable files = new Map();

    @computed get allFiles() {
        return [...this.files.values()]
    }

    @computed get selectedFile() {
        return this.allFiles.find(file => file.isSelected)
    }

    @action changeCode(filename, newCode) {
        const oldFile = this.files.get(filename);
        this.files.set(filename, {...oldFile, code: newCode})
    }

    @action addFiles(files) {
        files.forEach(file => {
            this.files.set(file.name, {...file, isSelected: false});
        })
    }

    @action selectFile(filename) {
        this.files.forEach((value, key, map) => {
            if (key !== filename) {
                map.set(key, {...value, isSelected: false})
            } else {
                map.set(key, {...value, isSelected: true})
            }
        });
    }

    @action reset() {
        this.files.clear();
    }
}

export default new editorStore();
