import {observable, action} from 'mobx';
import api from '../api';

class UserStore {

    @observable currentUser;
    @observable isLoading = false;

    @action setUser(user) {
        this.currentUser = user;
    }

    @action forgetUser() {
        this.currentUser = undefined;
    }

    @action checkUser() {
        this.isLoading = true;
        return api.Auth.init()
            .then(user => {
                this.currentUser = user;
            })
            .finally(() => {
                this.isLoading = false;
            })

    }
}

export default new UserStore();
