import {observable, action} from 'mobx'
import userStore from './userStore'
import api from '../api';


class authStore {
    @observable inProgress = false;
    @observable loginErrors = undefined;
    @observable signupErrors = undefined;

    @observable values = {
        username: '',
        email: '',
        password: '',
    };

    @action setUsername(username) {
        this.values.username = username;
    }

    @action setEmail(email) {
        this.values.email = email;
    }

    @action setPassword(password) {
        this.values.password = password;
    }

    @action reset() {
        this.values.username = '';
        this.values.email = '';
        this.values.password = '';
    }

    @action login() {
        this.inProgress = true;
        this.errors = undefined;
        return api.Auth.login(this.values)
            .then((user) => userStore.setUser(user))
            .catch((err) => {
                this.loginErrors = err;
                throw err
            })
            .finally(() => {
                this.inProgress = false;
            });
    }

    @action signup() {
        this.inProgress = true;
        this.errors = undefined;
        return api.Auth.signup(this.values)
            .catch((err) => {
                this.signupErrors = err;
                throw err
            })
            .finally(() => {
                this.inProgress = false;
            });
    }

    @action logout() {
        return api.Auth.logout()
            .then(() => userStore.forgetUser())
            .catch((err) => {
                throw err
            })
            .finally(action(() => {
                this.inProgress = false;
            }));
    }
}

export default new authStore();
