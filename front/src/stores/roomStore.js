import { observable, action, computed } from 'mobx';
import api from '../api';


class roomStore {

    @observable isLoading = false;
    @observable roomRegistry = new Map();

    @computed get rooms() {
        return [...this.roomRegistry.values()];
    };

    getRoom(id) {
        return this.roomRegistry.get(id);
    }

    @action loadRooms() {
        this.isLoading = true;
        return api.Rooms.list()
            .then(({ rooms }) => {
                this.roomRegistry.clear();
                rooms.forEach(room => this.roomRegistry.set(room.id, {...room, isEditing: false}));
            })
            .finally(() => { this.isLoading = false; });
    }

    @action createRoom(room) {
        return api.Rooms.create(room)
            .then(room => {
                this.roomRegistry.set(room.id, room);
                return Promise.resolve(room);
            })
            .catch((err) => {
                throw err
            })
    }

    @action updateRoom(updatedRoom) {
        const {id, name, description, users} = updatedRoom;
        return api.Rooms.update({id, name, description, users})
            .then((room) => {
                this.roomRegistry.set(room.id, {...room, isEditing: false});
                return room;
            })
            .catch((err) => {
                throw err
            })
    }

    @action deleteRoom(roomId) {
        return api.Rooms.delete(roomId)
            .then(() => {
                this.roomRegistry.delete(roomId)
            })
            .catch((err) => {
                throw err
            })
    }

    @action setEditing(roomId) {
        const oldRoom = this.roomRegistry.get(roomId);
        this.roomRegistry.set(roomId, {...oldRoom, isEditing: true})
    }

    @action hideEditing(roomId) {
        const oldRoom = this.roomRegistry.get(roomId);
        this.roomRegistry.set(roomId, {...oldRoom, isEditing: false})
    }

    @action reset() {
        this.roomRegistry.clear();
    }
}

export default new roomStore();
