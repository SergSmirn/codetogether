import {observable, action, computed, toJS} from 'mobx'
import roomStore from './roomStore'
import api from '../api'


class createRoomStore {
    @observable inProgress = false;
    @observable errors = undefined;

    @observable selectedUsersMap = new Map();
    @observable users = [];
    @observable files = [];

    @observable values = {name: '', description: '', project: {}};

    @computed get selectedUsers() {
        return [...this.selectedUsersMap.values()];
    }

    @computed get allUsers() {
        return toJS(this.users);
    }

    isUserSelected(user) {
        return !!this.selectedUsersMap.get(user.id);
    }

    @action loadUsers() {
        return api.Rooms.users()
            .then(({users}) => {
                this.users = users;
            })
    }

    @action addUser(user) {
        this.selectedUsersMap.set(user.id, user);
    }

    @action removeUser(userId) {
        this.selectedUsersMap.delete(userId);
    }

    @action setName(name) {
        if (!name) {
            this.errors = 'Это поле обязательно';
        } else {
            this.errors = undefined;
        }
        this.values.name = name;
    }

    @action setDescription(description) {
        this.values.description = description;
    }

    @action addProject(project) {
        this.values.project = {...this.values.project, ...project};
    }

    @action addFiles(files) {
        this.files = [...this.files, ...files];
    }

    @action clearForm() {
        this.values.name = '';
        this.values.description = '';
        this.files = [];
        this.selectedUsersMap.clear();
    }

    @action postForm() {
        this.inProgress = true;
        this.errors = undefined;
        return roomStore.createRoom({
            name: this.values.name,
            description: this.values.description,
            project: this.values.project,
            users: this.selectedUsers
        })
            .catch((err) => {
                throw err
            })
            .finally(() => {
                this.inProgress = false;
            })
    }
}

export default new createRoomStore();
